import React from 'react'
import Routes from './src/routes'

/** redux imports */
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import reducers from './src/redux/reducers'

const store = createStore(reducers)

const App = () => (
	<Provider store={store}>
		<Routes />
	</Provider>
)

export default App
