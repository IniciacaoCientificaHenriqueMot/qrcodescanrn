function loadingStart() {
	return { type: `LOADING`, payload: true }
}

function loadingStop() {
	return { type: `LOADING`, payload: false }
}
export { loadingStart, loadingStop }

const INITIAL_STATE = {
	loading: false,
}

export function BaseReducer(state = INITIAL_STATE, action) {
	switch (action.type) {
		case `LOADING`:
			return { ...state, loading: action.payload }
		default:
			return state
	}
}
