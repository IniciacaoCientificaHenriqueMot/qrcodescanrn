import { useDispatch } from 'react-redux'
import { loadingStart, loadingStop } from './reducer-base'

const sufix = 'QRCODE'

export function useQRCode() {
	const dispatch = useDispatch()
	return {
		informarQRCode(obj) {
			dispatch(loadingStart())
			dispatch({ type: `GET${sufix}`, payload: obj })
			dispatch(loadingStop())
		},
	}
}

const INITIAL_STATE = {
	qrcode: null,
}

export function QRCodeReducer(state = INITIAL_STATE, action) {
	switch (action.type) {
		case `GET${sufix}`:
			return { ...state, qrcode: action.payload }
		default:
			return state
	}
}
