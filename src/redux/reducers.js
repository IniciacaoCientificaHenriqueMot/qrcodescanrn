import * as actions from './actions'
import { combineReducers } from 'redux'

const reducerNames = Object.keys(actions).filter(a => a.substring(a.length - 7) === 'Reducer')

const reducers = {}
reducerNames.forEach(e => (reducers[e.substring(0, e.length - 7)] = actions[e]))

export default combineReducers(reducers)
