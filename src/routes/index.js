import React from 'react'
import { Router, Stack, Scene } from 'react-native-router-flux'

/** view imports */
import { Home } from '../views'

export default function Routes() {
	return <Logged />
}

function Logged(props) {
	return (
		<Router>
			<Stack>
				<Scene key='home' title='Home' component={Home} />
			</Stack>
		</Router>
	)
}

function NotLogged(props) {
	return (
		<Router>
			<Stack>
				<Scene key='home' component={Home} />
			</Stack>
		</Router>
	)
}
