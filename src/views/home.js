import React from 'react'
import { Text, SafeAreaView } from 'react-native'
import { useSelector } from 'react-redux'
import { useQRCode } from '../redux/actions'

export default function Home(props) {
	const { informarQRCode } = useQRCode()
	const { loading } = useSelector(state => state.Base)
	const { qrcode } = useSelector(state => state.QRCode)

	React.useEffect(() => {
		informarQRCode({ nome: 'henrique mota' })
	}, [])

	return (
		<SafeAreaView>
			{loading && <Text>Carregando</Text>}
			{qrcode && <Text>{qrcode.nome}</Text>}
		</SafeAreaView>
	)
}
