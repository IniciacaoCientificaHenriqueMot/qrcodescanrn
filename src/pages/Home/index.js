import React, { Component } from 'react'
import { TouchableOpacity, Text, StyleSheet, View, SafeAreaView, Image } from 'react-native'
import { Actions } from 'react-native-router-flux'
import LottieView from '../../../node_modules/lottie-react-native'

export default class index extends Component {
	static navigationOptions = {
		title: 'Home',
		header: null,
	}

	constructor(props) {
		super(props)
		this.state = {}
	}
	render() {
		return (
			<SafeAreaView style={styles.container}>
				<View style={{ height: 100, width: 250, justifyContent: 'center', alignItems: 'center' }}>
					<Text style={{ color: '#FFF', textAlign: 'center', fontWeight: 'bold', fontSize: 28 }}>
						Faça sua inscrição aqui e aproveite o melhor do Evento!
					</Text>
				</View>
				<View style={{ height: 200, width: 400, justifyContent: 'center', alignItems: 'center' }}>
					<LottieView resizeMethod='resize' source={require('../../../assets/qr-scan.json')} autoPlay loop />
				</View>
				<View>
					<TouchableOpacity
						style={styles.button}
						onPress={() => {
							Actions.scan()
						}}>
						<Text style={styles.textButton}>Escanear</Text>
					</TouchableOpacity>
				</View>
			</SafeAreaView>
		)
	}
}
const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#1F2347',
		alignItems: 'center',
		justifyContent: 'center',
	},
	button: {
		marginTop: 15,
		height: 40,
		width: 200,
		backgroundColor: '#006ae1',
		alignItems: 'center',
		justifyContent: 'center',
		borderRadius: 3,
	},
	textButton: {
		color: '#FFF',
	},
	logo: {
		alignItems: 'center',
		justifyContent: 'center',
		paddingBottom: 20,
	},
})
