import { StyleSheet } from 'react-native'
export const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#FFF',
		justifyContent: 'center',
		alignItems: 'center',
	},
	offset: {
		flex: 1,
		backgroundColor: 'rgba(0, 0, 0, 0.5)',
		position: 'absolute',
	},
	header: {
		textAlign: 'center',
		width: '70%',
		padding: 15,
		fontSize: 15,
	},
	vertical: {
		flex: 1,
		justifyContent: 'space-between',
	},
	horizontal: {
		flexDirection: 'row',
		alignContent: 'center',
		height: 200,
	},
	cor: {
		backgroundColor: 'rgba(0, 0, 0, 0.5)',
		height: 250,
	},
	buttonVoltar: {
		fontSize: 20,
		marginTop: 80,
		marginLeft: 15,
		color: '#fff',
	},
	Modalcontainer: {
		justifyContent: 'center',
		alignItems: 'center',
	},

	input: {
		width: '90%',
		height: 40,
		marginTop: 10,
		marginLeft: 10,
		backgroundColor: '#FFF',
		borderWidth: 1,
		borderColor: '#e3e3e3',
		borderRadius: 6,
		paddingLeft: 10,
	},
	button: {
		flexDirection: 'row',
	},
	buttonCancelar: {
		marginTop: 15,
		backgroundColor: '#e3e3e3',
		borderWidth: 1,
		borderColor: 'transparent',
		borderRadius: 6,
		padding: 10,
		alignItems: 'center',
	},
	buttonCadastrar: {
		marginTop: 15,
		backgroundColor: '#1E90FF',
		borderWidth: 1,
		borderColor: 'transparent',
		borderRadius: 6,
		padding: 10,
		color: '#FFF',
		marginLeft: 5,
	},
})
