import * as React from 'react'
import { Text, View, StyleSheet, TouchableOpacity, Modal, TextInput } from 'react-native'
import * as Permissions from 'expo-permissions'
import { connect } from 'react-redux'
import Icon from 'react-native-vector-icons/FontAwesome'
import { Actions } from 'react-native-router-flux'
import firebase from '../../services/firebaseConnection'

import { styles } from './styles'

import { BarCodeScanner } from 'expo-barcode-scanner'

import { editQrCode } from '../../store/actions/qr-code-action'

console.disableYellowBox = true

class LeitorQrCode extends React.Component {
	static navigationOptions = {
		title: 'Barcode',
		header: null,
		data: '',
	}
	constructor(props) {
		super(props)
		this.state = {
			matriculaInput: '123456789',
			nomeInput: 'Jeferson Sales ',
			cursoInput: 'Análise e desenvolvimento de sistemas',
			key: null,
			hasCameraPermission: null,
			barCodeData: null,
			type: null,
			modalVisible: false,
			dados: [],
		}
	}

	async componentDidMount() {
		this.getPermissionsAsync()
	}

	getPermissionsAsync = async () => {
		const { status } = await Permissions.askAsync(Permissions.CAMERA)
		this.setState({ hasCameraPermission: status === 'granted' })
	}

	handleBarCodeScanned = ({ type, data }) => {
		this.setState({
			modalVisible: true,
			dados: data,
			type: type,
		})
		alert(dados.key)
		this.props.editQrCode(data)
	}

	onCancel = () => {
		return this.setState({ modalVisible: false })
	}

	cadastrar = () => {
		const { matriculaInput, nomeInput, cursoInput, dados } = this.state

		if (matriculaInput === '' || nomeInput === '' || cursoInput === '') {
			Alert.alert('insira os dados')
		} else {
			alert(matriculaInput + nomeInput + cursoInput)
			const db = firebase.firestore()
			db.collection('inscritos').add({
				matricula: matriculaInput,
				nome: nomeInput,
				curso: cursoInput,
				data: new Date(),
				palestra: dados,
			})
			this.setState({
				matriculaInput: '',
				nomeInput: '',
				cursoInput: '',
				modalVisible: false,
			})
		}
	}

	render() {
		const { hasCameraPermission, modalVisible, matriculaInput, nomeInput, cursoInput, dados } = this.state

		// if (hasCameraPermission === null) {
		// 	return <Text>Nós precisamos de sua permissão para a câmera</Text>
		// }
		// if (hasCameraPermission === false) {
		// 	return <Text>Sem acesso a câmera</Text>
		// }
		return (
			<View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center', backgroundColor: '#000' }}>
				<BarCodeScanner
					onBarCodeScanned={modalVisible ? undefined : this.handleBarCodeScanned}
					style={StyleSheet.absoluteFillObject}>
					<View style={styles.vertical}>
						<View style={styles.cor}>
							<TouchableOpacity
								onPress={() => {
									Actions.pop()
								}}>
								<Text style={styles.buttonVoltar}>
									<Icon name='arrow-circle-left' size={28} color='#DDD' />
								</Text>
							</TouchableOpacity>
						</View>
						<View style={styles.horizontal}>
							<View style={styles.cor}></View>
							<View style={styles.cor}></View>
						</View>
						<View style={styles.cor}></View>
					</View>
				</BarCodeScanner>

				<Modal animationType='slide' visible={modalVisible} transparent={false}>
					<View style={styles.Modalcontainer}>
						<Text style={styles.header}>Adicione as infomações</Text>
						<TextInput
							placeholder='Matricula...'
							keyboardType={'numeric'}
							style={styles.input}
							value={matriculaInput}
							onChangeText={matriculaInput => this.setState({ matriculaInput })}
						/>
						<TextInput
							placeholder='Nome...'
							style={styles.input}
							value={nomeInput}
							onChangeText={nomeInput => this.setState({ nomeInput })}
						/>
						<TextInput
							placeholder='Curso...'
							style={styles.input}
							value={cursoInput}
							onChangeText={cursoInput => this.setState({ cursoInput })}
						/>
						<View style={styles.button}>
							<TouchableOpacity onPress={this.onCancel}>
								<Text style={styles.buttonCancelar}>Voltar</Text>
							</TouchableOpacity>
							<TouchableOpacity onPress={this.cadastrar}>
								<Text style={styles.buttonCadastrar}>Cadastrar</Text>
							</TouchableOpacity>
						</View>
					</View>
				</Modal>
			</View>
		)
	}
}

const mapDispatchToProps = {
	editQrCode,
}
export default connect(
	null,
	mapDispatchToProps,
)(LeitorQrCode)
